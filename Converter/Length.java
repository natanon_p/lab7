package Converter;
/**
 * An enum of length units that have different value.
 * @author Natanon Poonawagul
 *
 */
public enum Length implements Unit{
	/** A constant unit values */
	METER("meter",1.0),CENTIMETER("centimeter",0.01),MILLIMETER("millimeter",0.001),KILOMETER("kilometer",1000.0),MILE("mile",1609.344),FOOT("foot",0.30480),WA("wa",2.0);
	/** Name of this unit */
	public final String name;
	/** Value of this unit */
	public final double value;
	/**
	 * Constructor of this length unit.
	 * @param name of this unit.
	 * @param value of this unit.
	 */
	private Length(String name,double value){
		this.name = name;
		this.value = value;
	}
	/**
	 * A method convert amount with this unit to another unit.
	 * @param unit to converted.
	 * @param amount to be converted.
	 * @return amount that already converted.
	 */
	public double convertTo(Unit unit,double amount){
		double base = amount*this.value;
		return base/unit.getValue();
	}
	/**
	 * A method get the value from this unit.
	 * @return the value of this unit.
	 */
	public double getValue(){
		return this.value;
	}
	/**
	 * A method get the name of this unit.
	 * @return the name of this unit.
	 */
	public String toString(){
		return this.name;
	}
}
