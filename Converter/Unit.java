package Converter;
/**
 * An interface for the length units.
 * @author tannatanon
 *
 */
public interface Unit {
	/** A method use to convert the unit. */
	double convertTo(Unit unit,double amount );
	/** A method return value of the unit. */
	double getValue();
	/** A method return name of the unit. */
	String toString();
}
