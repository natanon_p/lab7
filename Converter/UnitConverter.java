package Converter;
/**
 * A class use as a unit converter.
 * @author Natanon Poonawagul
 *
 */
public class UnitConverter {
	/**
	 * A method use to convert amount from one unit to the another.
	 * @param amount is the amount to be converted.
	 * @param fromUnit is the unit of the amount.
	 * @param toUnit is the unit to convert.
	 * @return the amount that already converted.
	 */
	public double convert(double amount,Unit fromUnit,Unit toUnit){
		return fromUnit.convertTo(toUnit, amount);
	}
	/**
	 * A method use to get all the length units.
	 * @return the array of all length units.
	 */
	public Unit[] getUnit(){
		return new Unit[]{Length.METER,Length.KILOMETER,Length.MILE,Length.CENTIMETER,Length.FOOT,Length.WA,Length.MILLIMETER};
	}
}
