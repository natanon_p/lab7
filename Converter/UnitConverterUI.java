package Converter;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * A class that contain GUI for the Converter. 
 * @author Natanon Poonawagul
 *
 */
public class UnitConverterUI {
	JTextField txt1 = new JTextField(10);
	JComboBox box1 = new JComboBox();
	JTextField txt2 = new JTextField(10);
	JComboBox box2 = new JComboBox();
	/**
	 * A Constructor for this class.
	 */
	public UnitConverterUI(){
		this.init();
	}
	/**
	 * A method initialize GUI components for this class.
	 */
	public void init(){
		UnitConverter uc = new UnitConverter();
		Unit[] units = uc.getUnit();
		JFrame frame = new JFrame();
		JPanel pane = new JPanel();
		pane.setLayout(new FlowLayout());
		JLabel label = new JLabel("=");
		txt2.setEditable(false);
		for(int i = 0;i < units.length;i++){
			box1.addItem(units[i]);
			box2.addItem(units[i]);
		}
		JButton convert_btn = new JButton("Convert");
		JButton clear_btn = new JButton("Clear");
		convert_btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				try{
				if(Double.parseDouble(txt1.getText())<0){
					JOptionPane.showMessageDialog(null,"Cant Compute Negative Number","Error",JOptionPane.WARNING_MESSAGE);
				}
				else{
					txt2.setText(String.format("%.6f",uc.convert(Double.parseDouble(txt1.getText()), (Unit)box1.getSelectedItem(), (Unit)box2.getSelectedItem())));
				}
				} catch (NumberFormatException ex) {
						JOptionPane.showMessageDialog(null,"Please Insert Number","Error",JOptionPane.WARNING_MESSAGE);
					}
			}
		});
		clear_btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				txt1.setText("");
				txt2.setText("");
			}
		});
		pane.add(txt1);
		pane.add(box1);
		pane.add(label);
		pane.add(txt2);
		pane.add(box2);
		pane.add(convert_btn);
		pane.add(clear_btn);
		frame.add(pane);
		frame.pack();
		frame.setVisible(true);
	}
}
